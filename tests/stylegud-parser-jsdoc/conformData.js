import test from 'ava';
import { conformData } from 'stylegud-parser-jsdoc';

test('it should properly parse data tags', (t) => {
    t.plan(2);
    const inputs = [
        '{"a": "a"} - This is a test item',
        '[ "test2" ] - Another one'
    ];
    const expectations = [
        {
            name: '',
            description: 'This is a test item',
            datum: { a: 'a' }
        },
        {
            name: '',
            description: 'Another one',
            datum: [ 'test2' ]
        }
    ];

    inputs.forEach((item, key) => t.deepEqual(conformData(item), expectations[key]));
});

test('it should fail for invalid JSON', (t) => {
    t.plan(1);
    t.throws(() => conformData('{a:\'a\'} - wheee'));
});

test('it should allow data tags without description', (t) => {
    t.plan(1);
    const expected = {
        name: '',
        description: '',
        datum: { a: 'a' }
    };
    t.deepEqual(conformData('{"a": "a"}'), expected);
});
