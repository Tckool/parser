import test from 'ava';
import { setSections } from 'util/sections';

test('it should add a "sections" property to all sections', (t) => {
    t.plan(4);
    const sections = setSections([ {}, {} ]);
    sections.forEach((section) => {
        t.truthy(section.hasOwnProperty('sections'));
        t.deepEqual(section.sections, []);
    });
});

test('it should override an existing "sections" property', (t) => {
    t.plan(3);
    const sections = setSections([ { sections: 'a', reference: 'a' }, { reference: 'a.b' }, { reference: 'q' } ]);
    const expected = [
        [ 'a.b' ],
        [],
        []
    ];
    sections.forEach((section, idx) => t.deepEqual(section.sections, expected[idx]));
});
