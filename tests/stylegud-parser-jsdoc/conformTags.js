import test from 'ava';
import jsdoc from 'jsdoc-api';
import { conformTags } from 'stylegud-parser-jsdoc';

test.beforeEach(t => {
    t.context.input = jsdoc.explainSync({
        source: `/** example doclet
                  * @t1 test
                  * @t1 test2
                  * @t2 test3
                  */
                 var example = true`
    });
});

test('it should properly parse multiple tags', (t) => {
    t.plan(1);
    const parsed = conformTags(t.context.input[0].tags);
    t.deepEqual(parsed, {
        t1: [
            {
                originalTitle: 't1',
                title: 't1',
                text: 'test',
                value: 'test'
            },
            {
                originalTitle: 't1',
                title: 't1',
                text: 'test2',
                value: 'test2'
            }
        ],
        t2: [
            {
                originalTitle: 't2',
                title: 't2',
                text: 'test3',
                value: 'test3'
            }
        ]
    });
});
