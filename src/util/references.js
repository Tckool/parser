/**
 * Creates a referenceURI for all items in our dataset
 *
 * @param {Object[]} sections All the sections we've accumulated
 * @returns {Object[]} The sections
 */
function setReferences(sections) {
    sections.forEach((item) => {
        item.referenceURI = uriify(item.reference);
    });
    return sections;
}

/**
 * Converts a reference to a URI. Copied from KSS for consistency
 *
 * @link https://github.com/kss-node/kss-node/blob/e651226e04fb3c5755e68f7b3959d765c240358d/lib/kss_section.js#L347
 * @param {String} ref The reference to make URLable
 * @returns {String} The URIified reference
 */
function uriify(ref) {
    return ref.replace(/ \- /g, '-')
        .replace(/[^\w-]+/g, '-')
        .toLowerCase();
}

/**
 * Converts a string reference ('a.b.c') to a path array (['a', 'b', 'c'])
 *
 * @export
 * @param {String} reference The reference 'a.b.c'
 * @returns {String[]} The path ['a', 'b', 'c']
 */
function strToPath(reference) {
    return reference.split('.');
}

/**
 * Converts a path array (['a', 'b', 'c']), to a string ('a.b.c')
 *
 * @export
 * @param {String[]} path The path ['a', 'b', 'c']
 * @returns {String} The reference 'a.b.c'
 */
function pathToStr(path) {
    return path.join('.');
}

/**
 * Determines if a childPath is an "immediate child" of a basePath
 *
 * @export
 * @param {String[]} basePath The parent path
 * @param {String[]} childPath The possibly child path
 * @returns {Boolean} Whether or not the childPath is an immediate child of the basePath
 */
function isImmediateChild(basePath, childPath) {
    if (childPath.length <= basePath.length) {
        return false;
    }
    if (childPath.length > basePath.length + 1) {
        return false;
    }
    return basePath.every((segment, idx) => segment === childPath[idx]);
}

module.exports = { setReferences, uriify, strToPath, pathToStr, isImmediateChild };
