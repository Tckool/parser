const objectMapper = require('object-mapper');
const R = require('ramda');


/**
 * A wrapper over object-mapper to allow slightly nicer syntax (IMO)
 *
 * @param {Object} source The original object
 * @param {Object} map The map to use for creating a new object
 * @returns {Object} The mapped result
 */
function sgObjectMapper(source, map) {
    const targetKeys = Object.keys(map);
    map = swapKeys(R.clone(map));
    const result = objectMapper(source, map);
    const out = targetKeys
        .map((path) => path.trim('!?').split('.'))
        .reduce((acc, path) => R.assocPath(path, undefined, acc), {});
    return Object.assign({}, out, result);
}

/**
 * Swaps keys for cleaner object mapping (IMO)
 *
 * @param {Object} map The original map
 * @returns {Object} The map with keys swapped
 * @throws
 */
function swapKeys(map) {
    return Object.keys(map)
        .map((key) => [ key, R.clone(map[key]) ])
        .filter(([ key, value ]) => {
            if (!key) {
                throw Error('Wat');
            }
            if (R.isArrayLike(value)) {
                throw Error('Source field re-use is not currently supported');
            }
            if (R.is(Object, value) && !value.key) {
                throw Error('Missing key field on object syntax');
            }
            return Boolean(value);
        })
        .reduce((acc, [ key, value ]) => {
            if (R.is(String, value)) {
                acc[value] = key;
                return acc;
            }
            const valKey = value.key;
            value.key = key;
            const existing = acc[valKey];
            if (existing) {
                if (R.isArrayLike(existing)) {
                    existing.push(value);
                    return acc;
                }
                acc[valKey] = [ existing, value ];
                return acc;
            }
            acc[valKey] = value;
            return acc;
        }, {});
}

module.exports = { sgObjectMapper, swapKeys };
