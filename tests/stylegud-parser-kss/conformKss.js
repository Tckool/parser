import test from 'ava';
import { conformKss } from 'stylegud-parser-kss';

test('it should properly parse data', (t) => {
    t.plan(1);
    const input = {
        header: 'a',
        description: 'b',
        reference: '😁',
        markup: 'd',
        source: {
            filename: 'stylegud-parser-kss.js',
            path: 'src/',
            line: 12
        },
        modifiers: [
            {
                name: 'a',
                description: 'b',
                className: 'c'
            },
            {
                name: 'd',
                description: 'e',
                className: 'f'
            }
        ],
        renderer: 'q'
    };
    const actual = conformKss(input);
    const expected = {
        description: 'b',
        deprecated: undefined,
        experimental: undefined,
        header: 'a',
        markup: [ 'd' ],
        reference: '😁',
        renderer: 'q',
        source: {
            data: [
                {
                    className: 'c',
                    datum: 'c',
                    description: 'b',
                    name: 'a'
                },
                {
                    className: 'f',
                    datum: 'f',
                    description: 'e',
                    name: 'd'
                }
            ],
            filename: 'stylegud-parser-kss.js',
            line: 12,
            path: '.'
        }
    };
    t.deepEqual(actual, expected);
});
